#!/bin/bash

# shellcheck disable=SC2155
export SOURCE_DATE_EPOCH=$(git log -1 --format=%ct)

export STEAM_APP_ID_LIST="212050 307570 307580 336130 371800"

source lib.sh
